import java.util.HashMap;
import java.util.Scanner;

public class hash {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		HashMap<String, Integer> hashmap = new HashMap<>();
		String name;

		while (true) {
			name = kb.next();
			if (name.equals("*"))
				break;
			if (hashmap.containsKey(name))
				hashmap.put(name,hashmap.get(name) + 1);
			else
				hashmap.put(name, 1);
		}

		System.out.println(hashmap);
	}

}