import java.util.Scanner;

public class hashMap {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		LinearProblingHashST<String, Integer> st = new LinearProblingHashST<String, Integer>();
		String code;
		int price;
		while (true) {
			code = kb.next();
			if (code.equals("*"))
				break;
			price = kb.nextInt();
			st.put(code, price);
		}

		String num = kb.next();
		if (st.get(num) == null) {
			System.out.println("not found");
		} else {
			System.out.println(st.get(num));
		}
	}

}